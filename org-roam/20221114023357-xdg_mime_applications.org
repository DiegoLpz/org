:PROPERTIES:
:ID:       3c1719cc-2fd6-452c-b9fb-65b9188b1b93
:END:
#+title: XDG MIME applications
#+filetags:Unix Shell
* XDG MIME applications
Standard to manage and organize files launching
* References
+ [[https://wiki.archlinux.org/title/XDG_MIME_Applications#Shared_MIME_database][XDG mime applications on arch wiki]]
