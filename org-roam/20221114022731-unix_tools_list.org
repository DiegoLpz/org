:PROPERTIES:
:ID:       65260f72-02ed-4948-a58e-58706abbe0c0
:END:
#+title: Unix tools list
#+filetags: Unix Shell
* Unix tools list
** Administration
+ [[id:6b8e97e3-0167-4a20-8c7b-eeb412d074f3][Handlr mime files opener]]
+ [[id:fb2aa00b-9500-4d48-938f-d5746351670b][lf file manager]]
+ Search duplicate files using [[id:551677fe-e8c1-4b00-81d1-32844bf0017c][fdupes tool]] and [[id:918b124d-e463-4c78-b963-2860b1863af2][rmlint tool]]
