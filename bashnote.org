#+title: Bashnote

Clear example of differences between quote, double quote, and back quote on unix shell

[keko@bourbaki-ms7d30 ~]$ echo $myvar
10
[keko@bourbaki-ms7d30 ~]$ echo 'expr $myvar + 1'
expr $myvar + 1
[keko@bourbaki-ms7d30 ~]$ echo "expr $myvar + 1"
expr 10 + 1
[keko@bourbaki-ms7d30 ~]$ echo `expr $myvar + 1`
11

The following command search for pdfs and epubs books in the current directory and copies them to a folder
ls | rg '.*pdf|.*epub' | xargs -d '\n' cp -t $HOME/Documents/temp
